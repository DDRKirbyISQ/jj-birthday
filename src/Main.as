package {
	import net.flashpunk.Engine;
	import net.flashpunk.FP;
	import net.flashpunk.utils.Input;
	import net.flashpunk.utils.Key;
	
	[Frame(factoryClass="Preloader")]
	public class Main extends Engine {
		public static const DEBUG:Boolean = false;
		
		public function Main() {
			super(400, 300, 60, true);
			FP.screen.color = 0x000000;
			FP.screen.scale = 2.0;
			
			// Debug console.
			if (DEBUG) {
				FP.console.enable();
			}
			
			FP.randomizeSeed();
			
			// Start game.
			FP.world = new GameWorld();
		}
	}
}
