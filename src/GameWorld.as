package {
	import net.flashpunk.graphics.Image;
	import net.flashpunk.graphics.Text;
	import net.flashpunk.FP;
	import net.flashpunk.graphics.Emitter;
	import net.flashpunk.Sfx;
	import net.flashpunk.Tween;
	import net.flashpunk.Tweener;
	import net.flashpunk.utils.Ease;
	import net.flashpunk.World;
	import net.flashpunk.Entity;
	
	/**
	 * ...
	 * @author DDRKirby(ISQ)
	 */
	public class GameWorld extends World {
		[Embed(source='../img/particle_tiny.png')]
		private static const kParticleFile:Class;

		[Embed(source='../img/particle_large.png')]
		private static const kParticleFile2:Class;
		[Embed(source='../img/particle_medium.png')]
		private static const kParticleFile3:Class;
		[Embed(source='../img/particle_small.png')]
		private static const kParticleFile4:Class;
		[Embed(source='../img/star_particle.png')]
		private static const kParticleFile5:Class;
		[Embed(source='../img/particle_star.png')]
		private static const kParticleFile6:Class;
		
		private var _background:Image = new Image(kParticleFile);
		private var _background2:Image = new Image(kParticleFile);
		
		public static var Instance:GameWorld;
		
		public var Stars:Vector.<Star> = new Vector.<Star>();
		private var _emitter:Emitter = new Emitter(kParticleFile, 1, 1);
		private var _emitterTimer:int = 0;
		private var _emitter2:Emitter = new Emitter(kParticleFile5, 5, 4);
		
		private var _texts:Vector.<Text> = new Vector.<Text>();
		private var _shadowTexts:Vector.<Text> = new Vector.<Text>();
		private var _started:Boolean = false;
		
		private var _birthdayText:Text = new Text("Happy Birthday, JJ", 200, 225);
		private var _birthdayShadow1:Text;
		private var _birthdayShadow2:Text;
		private var _birthdayShadow3:Text;
		private var _birthdayShadow4:Text;
		
		[Embed(source='../sfx/complete.mp3')]
		private static const kSfxFile:Class;
		private var _completeSfx:Sfx = new Sfx(kSfxFile);
		
		private var _completeTimer:int = 0;
		
		public function GameWorld() {
			Instance = this;
			_background.alpha = 0;
			_background.scaleX = 400;
			_background.scaleY = 300;
			_background.color = 0xffff90;
			addGraphic(_background, 1000);
			_background2.alpha = 0;
			_background2.scaleX = 400;
			_background2.scaleY = 300;
			_background2.color = 0xffffB0;
			addGraphic(_background2, -10000);
			
			Stars.push(new Star(235, 140, 0));
			Stars.push(new Star(175, 50, 1));
			Stars.push(new Star(155, 150, 2));
			Stars.push(new Star(300, 110, 3));
			Stars.push(new Star(110, 140, 4));
			Stars.push(new Star(300, 50, 5));
			Stars.push(new Star(280, 150, 6));
			Stars.push(new Star(175, 110, 7));
			for each (var star:Star in Stars) {
				star.y += 10;
				
				//star.Show();
			}
			for each (star in Stars) {
				add(star);
			}
			Stars[0].Show();
			addGraphic(_emitter);
			_emitter.newType("star1", [0]);
			_emitter.setMotion("star1", 0, 0, 100, 360, 0, 100);
			_emitter.setAlpha("star1", 1, 0, Ease);

			_emitter.newType("star2", [0]);
			_emitter.setMotion("star2", 0, 0, 100, 360, 0, 100);
			_emitter.setAlpha("star2", 1, 0, Ease);
			_emitter.setColor("star2", 0xFFFF00, 0xFFFF00);

			_emitter.newType("star1b", [0]);
			_emitter.setMotion("star1b", 0, 25, 100, 360, 50, 100);
			_emitter.setAlpha("star1b", 1, 0, Ease);

			_emitter.newType("star2b", [0]);
			_emitter.setMotion("star2b", 0, 25, 100, 360, 50, 100);
			_emitter.setAlpha("star2b", 1, 0, Ease);
			_emitter.setColor("star2b", 0xFFFF00, 0xFFFF00);

			addGraphic(_emitter2, 100);
			_emitter2.newType("star1", [0]);
			_emitter2.setMotion("star1", 0, 90, 100, 360, 120, 100);
			_emitter2.setAlpha("star1", 1, 0, Ease);

			_emitter2.newType("star2", [0]);
			_emitter2.setMotion("star2", 0, 90, 100, 360, 120, 100);
			_emitter2.setAlpha("star2", 1, 0, Ease);
			_emitter2.setColor("star2", 0x90FFFF, 0x90FFFF);

			_emitter2.newType("star3", [0]);
			_emitter2.setMotion("star3", 0, 90, 100, 360, 120, 100);
			_emitter2.setAlpha("star3", 1, 0, Ease);
			_emitter2.setColor("star3", 0xFFFF00, 0xFFFF00);

			_emitter2.newType("star4", [0]);
			_emitter2.setMotion("star4", 0, 90, 100, 360, 120, 100);
			_emitter2.setAlpha("star4", 1, 0, Ease);
			_emitter2.setColor("star4", 0x90FF90, 0x90FF90);

			for (var i:int = 0; i < 5; ++i) {
				_emitter.emit("star1", FP.random * FP.width, FP.random * FP.height);
				_emitter.emit("star2", FP.random * FP.width, FP.random * FP.height);
			}
			
			_texts.push(new Text("Dear JJ,", 100, 180));
			_texts.push(new Text("You are very pretty!", 100, 190));
			_texts.push(new Text("And really awesome!", 100, 200));
			_texts.push(new Text("Your hair is so long and beautiful...", 100, 210));
			_texts.push(new Text("And you are an amazing dancer.", 100, 220));
			_texts.push(new Text("We all love you,", 100, 230));
			_texts.push(new Text("So please enjoy your special day.", 100, 240));
			_texts.push(new Text("", 100, 250));
			for (i = 0; i < _texts.length; ++i) {
				_texts[i].x = Stars[i].x;
				_texts[i].y = Stars[i].y + 20;
			}
			for each (var text:Text in _texts) {
				text.scale = 0.5;
				text.alpha = 0.0;
				text.centerOrigin();
				var shadow:Entity = new Entity(0, 0);
				_shadowTexts.push(ShadowText.AddShadowText1(text, shadow, 1));
				_shadowTexts.push(ShadowText.AddShadowText2(text, shadow, 1));
				_shadowTexts.push(ShadowText.AddShadowText3(text, shadow, 1));
				_shadowTexts.push(ShadowText.AddShadowText4(text, shadow, 1));
				shadow.layer = -8500;
				add(shadow);
				var textEntity:Entity = new Entity(0, 0, text);
				textEntity.layer = -9000;
				add(textEntity);
			}
				_birthdayText.alpha = 0.0;
				_birthdayText.centerOrigin();
				var shadow2:Entity = new Entity(0, 0);
				_birthdayShadow1 = (ShadowText.AddShadowText1(_birthdayText, shadow2, 2));
				_birthdayShadow2 = (ShadowText.AddShadowText2(_birthdayText, shadow2, 2));
				_birthdayShadow3 = (ShadowText.AddShadowText3(_birthdayText, shadow2, 2));
				_birthdayShadow4 = (ShadowText.AddShadowText4(_birthdayText, shadow2, 2));
				shadow2.layer = -8500;
				add(shadow2);
				var textEntity2:Entity = new Entity(0, 0, _birthdayText);
				textEntity2.layer = -9000;
				add(textEntity2);
		}
		
		private function Ease(t:Number):Number {
			if (t  < 0.5) return 1 - t * 2;
			else return (t - 0.5) * 2;
		}
		
		override public function update():void {
			super.update();
			
			if (!_started && Stars[0].Enabled) {
				for (var i:int = 0; i < Stars.length; ++i) {
					Stars[i].Start();
					_started = true;
				}
			}
			
			for (i = 0; i < Stars.length - 1; ++i) {
				if (Stars[i].Enabled) {
					Stars[i + 1].Show();
				}
			}

			var starsAllClicked:Boolean = true;
			for (i = 0; i < Stars.length; ++i) {
				starsAllClicked = starsAllClicked && Stars[i].Clicked;
			}
			if (!starsAllClicked) {
				for (var i:int = 0; i < Stars.length; ++i) {
					if (!Stars[i].Enabled && Stars[i].Clicked) {
						Stars[i].DoEmit();
					}
				}
			} else {
				_birthdayText.alpha += 0.01;
				
				if (_completeTimer == 0) {
					_completeSfx.play();
				}
				_completeTimer++;

				if (_completeTimer < 15) {
					_background2.alpha += 1.0 / 30;
				} else {
					_background2.alpha -= 1.0 / 240;
				}
			}

			var starsAllEnabled:Boolean = true;
			for (i = 0; i < Stars.length; ++i) {
				starsAllEnabled = starsAllEnabled && Stars[i].Enabled;
			}
			if (starsAllEnabled) {
				_background.alpha = Math.min(_background.alpha + 0.003, 0.1);
				if (_emitterTimer % 15 == 0) {
					_emitter2.emit("star1", FP.random * FP.width, FP.random * FP.height);
					_emitter2.emit("star2", FP.random * FP.width, FP.random * FP.height);
					_emitter2.emit("star3", FP.random * FP.width, FP.random * FP.height);
					_emitter2.emit("star4", FP.random * FP.width, FP.random * FP.height);
				}
			} else {
				_background.alpha -= 0.003;
			}
			
			for (i = 0; i < _texts.length; ++i) {

				if (Stars[i].Clicked) {
					Stars[i].TextTimer++;
				}
				if (Stars[i].TextTimer > 0 && Stars[i].TextTimer < 180) {
					_texts[i].alpha += 0.03;
				} else {
					_texts[i].alpha -= 0.03;
				}
				
				_shadowTexts[i*4].alpha = _texts[i].alpha;
				_shadowTexts[i*4+1].alpha = _texts[i].alpha;
				_shadowTexts[i*4+2].alpha = _texts[i].alpha;
				_shadowTexts[i*4+3].alpha = _texts[i].alpha;
			}
			
			_birthdayShadow1.alpha = _birthdayText.alpha;
			_birthdayShadow2.alpha = _birthdayText.alpha;
			_birthdayShadow3.alpha = _birthdayText.alpha;
			_birthdayShadow4.alpha = _birthdayText.alpha;
			
			++_emitterTimer;
			if (_emitterTimer % 20 == 0) {
				//if (starsAllClicked) {
					//_emitter.emit("star1b", FP.random * FP.width, FP.random * FP.height);
//					_emitter.emit("star2b", FP.random * FP.width, FP.random * FP.height);
				//} else {
					_emitter.emit("star1", FP.random * FP.width, FP.random * FP.height);
					_emitter.emit("star2", FP.random * FP.width, FP.random * FP.height);
				//}
			}
		}
	}
}
