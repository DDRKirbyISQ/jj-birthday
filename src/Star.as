package {
	import net.flashpunk.Entity;
	import net.flashpunk.FP;
	import net.flashpunk.Graphic;
	import net.flashpunk.graphics.Emitter;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.Mask;
	import net.flashpunk.Sfx;
	import net.flashpunk.utils.Ease;
	import net.flashpunk.utils.Input;
	
	/**
	 * ...
	 * @author DDRKirby(ISQ)
	 */
	public class Star extends Entity {
		[Embed(source='../img/star.png')]
		private static const kImageFile:Class;
		private var _image:Image = new Image(kImageFile);
		
		[Embed(source='../img/glow.png')]
		private static const kGlowImageFile:Class;
		private var _glowImage:Image = new Image(kGlowImageFile);
		
		[Embed(source='../mus/jj-birthday_1.mp3')]
		private static const kMusicFile1:Class;
		[Embed(source='../mus/jj-birthday_2.mp3')]
		private static const kMusicFile2:Class;
		[Embed(source='../mus/jj-birthday_6.mp3')]
		private static const kMusicFile3:Class;
		[Embed(source='../mus/jj-birthday_7.mp3')]
		private static const kMusicFile4:Class;
		[Embed(source='../mus/jj-birthday_3.mp3')]
		private static const kMusicFile5:Class;
		[Embed(source='../mus/jj-birthday_4.mp3')]
		private static const kMusicFile6:Class;
		[Embed(source='../mus/jj-birthday_4-nosidechain.mp3')]
		private static const kMusicFile6b:Class;
		[Embed(source='../mus/jj-birthday_8.mp3')]
		private static const kMusicFile7:Class;
		[Embed(source='../mus/jj-birthday_8-nosidechain.mp3')]
		private static const kMusicFile7b:Class;
		[Embed(source='../mus/jj-birthday_5.mp3')]
		private static const kMusicFile8:Class;
		
		private static var kMusicFiles:Array = [kMusicFile1, kMusicFile2, kMusicFile3, kMusicFile4, kMusicFile5, kMusicFile6, kMusicFile7, kMusicFile8];
		
		[Embed(source='../sfx/star_1.mp3')]
		private static const kSfxFile1:Class;
		[Embed(source='../sfx/star_2.mp3')]
		private static const kSfxFile2:Class;
		[Embed(source='../sfx/star_3.mp3')]
		private static const kSfxFile3:Class;
		[Embed(source='../sfx/star_4.mp3')]
		private static const kSfxFile4:Class;
		
		private static var kSfxFiles:Array = [kSfxFile1, kSfxFile2, kSfxFile3, kSfxFile4];
		
		[Embed(source='../sfx/appear_1.mp3')]
		private static const kAppearFile1:Class;
		[Embed(source='../sfx/appear_2.mp3')]
		private static const kAppearFile2:Class;
		[Embed(source='../sfx/appear_3.mp3')]
		private static const kAppearFile3:Class;
		[Embed(source='../sfx/appear_4.mp3')]
		private static const kAppearFile4:Class;
		[Embed(source='../sfx/appear_5.mp3')]
		private static const kAppearFile5:Class;
		[Embed(source='../sfx/appear_6.mp3')]
		private static const kAppearFile6:Class;
		[Embed(source='../sfx/appear_7.mp3')]
		private static const kAppearFile7:Class;
		[Embed(source='../sfx/appear_8.mp3')]
		private static const kAppearFile8:Class;
		
		private static var kAppearFiles:Array = [kAppearFile1, kAppearFile2, kAppearFile3, kAppearFile4, kAppearFile5, kAppearFile6, kAppearFile7, kAppearFile8];
		
		[Embed(source='../img/star_particle.png')]
		private static const kParticleFile:Class;
		
		private var _music:Sfx;
		private var _musicB:Sfx;
		public var Enabled:Boolean = false;
		private var _emitter:Emitter = new Emitter(kParticleFile, 5, 4);
		private var _particleTimer:int = 0;
		private var _visible:Boolean = false;
		public var ShowTimer:int = 0;
		public var Clicked:Boolean = false;
		private var _number:int;
		public var TextTimer:int = 0;
		
		public function Star(x:Number, y:Number, number:int, startEnabled:Boolean = false) {
			super(x, y, _image);
			_number = number;
			setHitbox(_image.width + 10, _image.height + 10, (_image.width + 10) / 2 + 1, (_image.height + 10) / 2 + 1);
			_image.centerOrigin();
			_music = new Sfx(kMusicFiles[number]);
			if (number == 5) {
				_musicB = new Sfx(kMusicFile6b);
			}
			if (number == 6) {
				_musicB = new Sfx(kMusicFile7b);
			}
			
			addGraphic(_emitter);
			_emitter.newType("star1", [0]);
			_emitter.setMotion("star1", 0, 30, 30, 360, 30, 30);
			_emitter.setAlpha("star1", 1, 0);
			_emitter.setColor("star1", 0xffff90, 0xffff90);
			_emitter.newType("star2", [0]);
			_emitter.setMotion("star2", 0, 30, 30, 360, 30, 30);
			_emitter.setAlpha("star2", 1, 0);
			_emitter.setColor("star2", 0x90ff90, 0x90ff90);
			_emitter.newType("star3", [0]);
			_emitter.setMotion("star3", 0, 30, 30, 360, 30, 30);
			_emitter.setAlpha("star3", 1, 0);
			_emitter.setColor("star3", 0x90ffff, 0x90ffff);
			addGraphic(_glowImage);
			_glowImage.centerOrigin();
			_glowImage.alpha = 0;
			_image.alpha = 0;
			Enabled = startEnabled;
			_visible = startEnabled;
			Clicked = startEnabled;
			if (_number == 0) {
				ShowTimer = 160;
			}
		}
		
		public function Start():void {
			_music.loop(0.0);
			if (_musicB) {
				_musicB.loop(0.0);
			}
			if (_number == 0) {
				_music.volume = 1;
			}
		}
		
		public function Show():void {
			_visible = true;
		}
		
		public function Toggle():void {
			Enabled = !Enabled;
			if (Enabled) {
				new Sfx(kSfxFiles[FP.rand(kSfxFiles.length)]).play(0.5);
			}
			Clicked = true;
		}
		
		public function DoEmit():void {
			if (_particleTimer % 60 == 0) {
				_emitter.emit("star1", -1, -1);
			}
			if (_particleTimer % 60 == 20) {
				_emitter.emit("star2", -1, -1);
			}
			if (_particleTimer % 60 == 40) {
				_emitter.emit("star3", -1, -1);
			}			
		}
		
		override public function update():void {
			super.update();
			
			if (!_visible) {
				_image.alpha = 0.0;
				_glowImage.alpha = 0.0;
				return;
			}
			
			++_particleTimer;
			if (Enabled) {
				if (_particleTimer % 45 == 0) {
					_emitter.emit("star1", -1, -1);
				}
				if (_particleTimer % 45 == 15) {
					_emitter.emit("star2", -1, -1);
				}
				if (_particleTimer % 45 == 30) {
					_emitter.emit("star3", -1, -1);
				}
				
				if (_number == 5) {
					if (GameWorld.Instance.Stars[4].Enabled) {
						_music.volume = Math.min(0.75, _music.volume += 0.05);
						_musicB.volume -= 0.05;
					} else {
						_musicB.volume = Math.min(0.75, _musicB.volume += 0.05);
						_music.volume -= 0.05;
					}
				} else if (_number == 6) {
					if (GameWorld.Instance.Stars[4].Enabled) {
						_music.volume = Math.min(0.75, _music.volume += 0.05);
						_musicB.volume -= 0.05;
					} else {
						_musicB.volume = Math.min(0.75, _musicB.volume += 0.05);
						_music.volume -= 0.05;
					}
				} else {
					_music.volume = Math.min(0.75, _music.volume += 0.05);
					if (_musicB) _musicB.volume = Math.min(0.75, _musicB.volume += 0.05);
				}
				
				_glowImage.alpha += 0.05;
				_image.alpha += 0.05;
			} else {
				_music.volume -= 0.05;
				if (_musicB) _musicB.volume -= 0.05;
				_glowImage.alpha -= 0.05;
				if (_image.alpha > 0.3) {
					_image.alpha = Math.max(_image.alpha - 0.05, 0.3);
				}
			}
			if (!Clicked) {
				ShowTimer++;
				if (ShowTimer == 220) {
					new Sfx(kAppearFiles[_number]).play();
				}
				if (ShowTimer > 220 && ShowTimer < 240) {
					_image.alpha = Math.min(_image.alpha + 0.04, 1.0);
				}
				if (ShowTimer > 240) {
					_image.alpha = Math.max(_image.alpha - 0.005, 0.2);
				}
				if (ShowTimer > 400 && !Clicked) {
					if (_particleTimer % 60 == 0) {
						_emitter.emit("star1", -1, -1);
					}
					if (_particleTimer % 60 == 20) {
						_emitter.emit("star2", -1, -1);
					}
					if (_particleTimer % 60 == 40) {
						_emitter.emit("star3", -1, -1);
					}
				}
			}
			
			if (!Enabled && ShowTimer > 220 && collidePoint(x, y, Input.mouseX, Input.mouseY)) {
				if (_glowImage.alpha <= 0.25) {
					_glowImage.alpha = Math.min(_glowImage.alpha + 0.1, 0.25);
				}
			}
			
			if (Input.mousePressed && ShowTimer > 220) {
				if (collidePoint(x, y, Input.mouseX, Input.mouseY)) {
					Toggle();
				}
			}
		}
	}
}
